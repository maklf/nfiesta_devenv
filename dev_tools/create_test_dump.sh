pg_dump -d nfi_esta --data-only --format plain --file ../../nfiesta_pg/sql/full_stack_test_data.sql \
--table nfiesta_test.c_area_domain 			--table nfiesta_test.c_area_domain_id_seq \
--table nfiesta_test.c_area_domain_category 		--table nfiesta_test.c_area_domain_category_id_seq \
--table nfiesta_test.c_auxiliary_variable 		--table nfiesta_test.c_auxiliary_variable_id_seq \
--table nfiesta_test.c_auxiliary_variable_category 	--table nfiesta_test.c_auxiliary_variable_category_id_seq \
--table nfiesta_test.c_country 				--table nfiesta_test.c_country_id_seq \
--table nfiesta_test.c_estimation_cell 			--table nfiesta_test.c_estimation_cell_id_seq \
--table nfiesta_test.c_estimation_cell_collection 	--table nfiesta_test.c_estimation_cell_collection_id_seq \
--table nfiesta_test.cm_cluster2panel_mapping 		--table nfiesta_test.cm_cluster2panel_mapping_id_seq \
--table nfiesta_test.cm_plot2cell_mapping 		--table nfiesta_test.cm_plot2cell_mapping_id_seq \
--table nfiesta_test.cm_plot2cluster_config_mapping 	--table nfiesta_test.cm_plot2cluster_config_mapping_id_seq \
--table nfiesta_test.cm_refyearset2panel_mapping 	--table nfiesta_test.cm_refyearset2panel_mapping_id_seq \
--table nfiesta_test.c_state_or_change 			--table nfiesta_test.c_state_or_change_id_seq \
--table nfiesta_test.c_sub_population 			--table nfiesta_test.c_sub_population_id_seq \
--table nfiesta_test.c_sub_population_category 		--table nfiesta_test.c_sub_population_category_id_seq \
--table nfiesta_test.c_target_variable 			--table nfiesta_test.c_target_variable_id_seq \
--table nfiesta_test.c_variable_type 			--table nfiesta_test.c_variable_type_id_seq \
--table nfiesta_test.f_p_plot 				--table nfiesta_test.f_p_plot_gid_seq \
--table nfiesta_test.t_auxiliary_data 			--table nfiesta_test.t_auxiliary_data_id_seq \
--table nfiesta_test.t_cluster 				--table nfiesta_test.t_cluster_id_seq \
--table nfiesta_test.t_cluster_configuration 		--table nfiesta_test.t_cluster_configuration_id_seq \
--table nfiesta_test.t_inventory_campaign 		--table nfiesta_test.t_inventory_campaign_id_seq \
--table nfiesta_test.t_panel 				--table nfiesta_test.t_panel_id_seq \
--table nfiesta_test.t_plot_measurement_dates 		--table nfiesta_test.t_plot_measurement_dates_id_seq \
--table nfiesta_test.t_reference_year_set 		--table nfiesta_test.t_reference_year_set_id_seq \
--table nfiesta_test.t_strata_set 			--table nfiesta_test.t_strata_set_id_seq \
--table nfiesta_test.t_stratum 				--table nfiesta_test.t_stratum_id_seq \
--table nfiesta_test.t_target_data 			--table nfiesta_test.t_target_data_id_seq \

pg_dump -d nfi_esta --data-only --format plain --file ../../nfiesta_pg/sql/full_stack_test_conf.sql \
--table nfiesta_test.t_variable 			--table nfiesta_test.t_variable_id_seq \
--table nfiesta_test.t_panel2aux_conf 			--table nfiesta_test.t_panel2aux_conf_id_seq \
--table nfiesta_test.t_aux_total 			--table nfiesta_test.t_aux_total_id_seq \
--table nfiesta_test.t_model 				--table nfiesta_test.t_model_id_seq \
--table nfiesta_test.t_model_variables 			--table nfiesta_test.t_model_variables_id_seq \
--table nfiesta_test.cm_cell2param_area_mapping 	--table nfiesta_test.cm_cell2param_area_mapping_id_seq \
--table nfiesta_test.cm_plot2param_area_mapping 	--table nfiesta_test.cm_plot2param_area_mapping_id_seq \
--table nfiesta_test.c_param_area_type 			--table nfiesta_test.c_param_area_type_id_seq \
--table nfiesta_test.f_a_cell 				--table nfiesta_test.f_a_cell_gid_seq \
--table nfiesta_test.f_a_param_area 			--table nfiesta_test.f_a_param_area_gid_seq \
--table nfiesta_test.t_aux_conf 			--table nfiesta_test.t_aux_conf_id_seq \
#--table nfiesta_test.t_total_estimate_conf 		--table nfiesta_test.t_total_estimate_conf_id_seq \
#--table nfiesta_test.t_total_estimate_data 		--table nfiesta_test.t_total_estimate_data_id_seq \
#--table nfiesta_test.t_panel2total_1stph_est_data 	--table nfiesta_test.t_panel2total_1stph_est_data_id_seq \
#--table nfiesta_test.t_panel2total_2ndph_est_data 	--table nfiesta_test.t_panel2total_2ndph_est_data_id_seq \
#--table nfiesta_test.t_estimate_conf 			--table nfiesta_test.t_estimate_conf_id_seq \

#pg_dump -d contrib_regression --data-only --format plain --file ../../nfiesta_pg/sql/full_stack_test_results.sql \
#--table nfiesta_test.t_result \
#--table nfiesta_test.t_result_id_seq \
#--table nfiesta_test.t_g_beta \
#--table nfiesta_test.t_g_beta_id_seq \

sed -i '1s/^/--\n-- Copyright 2017, 2019 ÚHÚL\n--\n-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");\n-- You may not use this work except in compliance with the Licence.\n-- You may obtain a copy of the Licence at:\n--\n-- https:\/\/joinup.ec.europa.eu\/software\/page\/eupl\n--\n-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,\n-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n-- See the Licence for the specific language governing permissions and limitations under the Licence.\n--\n\n/' \
../../nfiesta_pg/sql/full_stack_test_data.sql ../../nfiesta_pg/sql/full_stack_test_conf.sql #../../nfiesta_pg/sql/full_stack_test_results.sql

sed -i '14s/^/-- DATA DISCLAIMER\n-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data\n-- do not reflect the true status or changes within any geographical area of the Czech Republic,\n-- at, during or between any time occasion(s).\n-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,\n-- be it a past or future CZNFI publication.\n--\n/' \
../../nfiesta_pg/sql/full_stack_test_data.sql
