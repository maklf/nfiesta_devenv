#---------------------------------UNLINK EXTENSION OBJECTS
select --*, 
	concat('alter extension diabolo drop table ', objid::regclass, ';' ) as alter_command
from pg_depend
where refobjid = 20813 /*OID of extension diabolo*/ and classid = 1259 /*tables and sequences*/
and substring(objid::regclass::text from '....$') != '_seq' 
order by objid::regclass::text;
\gexec

alter extension diabolo drop materialized view analytical.v_ldsity_conf;

select --*, 
	concat('alter extension diabolo drop sequence ', objid::regclass, ';' ) as alter_command
from pg_depend
where refobjid = 20813 /*OID of extension diabolo*/ and classid = 1259 /*tables and sequences*/
and substring(objid::regclass::text from '....$') = '_seq'
order by objid::regclass::text;
\gexec

select --*, 
	concat('alter extension diabolo drop function analytical.', proname, ';' ) as alter_command
from pg_depend inner join pg_proc on (oid = objid )
where refobjid = 20813 /*OID of extension diabolo*/ and classid = 1255 /*tables and sequences*/
order by objid::regclass::text;
\gexec

alter extension diabolo drop schema analytical ;

#---------------------------------STRUCTURE
PGPASSWORD=<insert your password here> pg_dump -h localhost -p 5432 -d diabolo_db -U <insert your user here> --schema analytical --schema-only --format plain --no-owner -f nfiesta--2.0.4.sql

#---------------------------------STATIC DATA
PGPASSWORD=<insert your password here> pg_dump -h localhost -p 5432 -d diabolo_db -U <insert your user here> \
--table analytical.c_estimate_type \
--table analytical.c_aux_phase_type \
--data-only --format plain --no-owner --column-inserts >> nfiesta--2.0.4.sql

#---------------------------------MARK DYNAMIC TABLES (dynamic are all except static)
#select --*, 
#	concat('SELECT pg_catalog.pg_extension_config_dump(''', objid::regclass, ''''', '''');' ) as alter_command
#from pg_depend
#where refobjid = 20813 /*OID of extension diabolo*/ and classid = 1259 /*tables and sequences*/
#order by objid::regclass::text;

echo "
SELECT pg_catalog.pg_extension_config_dump('analytical.c_area_domain', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_area_domain_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_area_domain_category', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_area_domain_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_auxiliary_variable', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_auxiliary_variable_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_auxiliary_variable_category', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_auxiliary_variable_category_id_seq', '');
--SELECT pg_catalog.pg_extension_config_dump('analytical.c_aux_phase_type', '');
--SELECT pg_catalog.pg_extension_config_dump('analytical.c_aux_phase_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_country', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_country_id_seq', '');
--SELECT pg_catalog.pg_extension_config_dump('analytical.c_estimate_type', '');
--SELECT pg_catalog.pg_extension_config_dump('analytical.c_estimate_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_estimation_cell', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_estimation_cell_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_estimation_cell_collection', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_estimation_cell_collection_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_cell2param_area_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_cell2param_area_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_cluster2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_cluster2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_plot2cell_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_plot2cell_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_plot2cluster_config_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_plot2cluster_config_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_plot2param_area_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_plot2param_area_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_refyearset2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.cm_refyearset2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_param_area_type', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_param_area_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_state_or_change', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_state_or_change_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_sub_population', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_sub_population_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_sub_population_category', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_sub_population_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_target_variable', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_target_variable_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_variable_type', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.c_variable_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.f_a_cell', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.f_a_cell_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.f_a_param_area', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.f_a_param_area_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.f_p_plot', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.f_p_plot_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_aux_conf', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_aux_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_auxiliary_data', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_auxiliary_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_aux_total', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_aux_total_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_cluster', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_cluster_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_cluster_configuration', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_cluster_configuration_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_g_beta', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_g_beta_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_inventory_campaign', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_inventory_campaign_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_model', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_model_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_model_variables', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_model_variables_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel2aux_conf', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel2aux_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel2total_1stph_est_data', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel2total_1stph_est_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel2total_2ndph_est_data', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_panel2total_2ndph_est_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_plot_measurement_dates', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_plot_measurement_dates_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_reference_year_set', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_reference_year_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_result', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_result_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_strata_set', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_strata_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_stratum', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_stratum_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_target_data', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_target_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_total_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_total_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_total_estimate_data', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_total_estimate_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_variable', '');
SELECT pg_catalog.pg_extension_config_dump('analytical.t_variable_id_seq', '');
">> nfiesta--2.0.4.sql

sed -i 's/analytical./@extschema@./g' nfiesta--2.0.4.sql
