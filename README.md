# gitlab CI/CD environment

## get docker image

Create docker image

```bash
docker build -t registry.gitlab.com/nfiesta/nfiesta_devenv:pg12 .
docker push registry.gitlab.com/nfiesta/nfiesta_devenv:pg12
```

or pull directly from repository

```bash
docker pull registry.gitlab.com/nfiesta/nfiesta_devenv:pg12
```

run image locally
```bash
docker image ls
docker run -i -t -v `pwd`:/home/vagrant/work_dir -p 5432:5432 --name nfiesta_dev --hostname nfiesta_dev registry.gitlab.com/nfiesta/nfiesta_devenv:pg12 /bin/bash
docker container ls -a
docker container rm -v nfiesta_dev
```

set-up local environment and run CI/CD jobs locally 
```
sudo dpkg-reconfigure tzdata
# git clone <dotfiles>
# sudo apt install ssh && set up or copy ssh keys && chmod 700 ~/.ssh/id_rsa
sudo mkdir -p /builds/nfiesta
sudo chown -R vagrant /builds
cd /builds/nfiesta
git clone git@gitlab.com:nfiesta/nfiesta_htc.git
git clone git@gitlab.com:nfiesta/nfiesta_pg.git
sudo -u postgres psql -c "create user vagrant with superuser;"
# sudo make install
# make installcheck
```

run CI/CD jobs locally (gitlab-runner)
```bash
gitlab-runner exec docker build1
gitlab-runner exec docker --docker-volumes `pwd`/build-output:/builds test1
docker rm `docker ps -aq`
docker system prune -a
```
