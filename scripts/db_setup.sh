#!/bin/sh -e
# Edit the following to change the name of the database user that will be created:
APP_DB_USER=adm_nfiesta
APP_DB_PASS=local_nfiesta_passwd

# Edit the following to change the name of the database that is created (defaults to the user name)
APP_DB_NAME=nfiesta_db

PG_VERSION=11
###########################################################
# Changes below this line are probably not necessary
###########################################################
print_db_usage () {
  echo "Your PostgreSQL database has been setup and can be accessed on your local machine on the forwarded port (default: 15432)"
  echo "  Host: localhost"
  echo "  Port: 15432"
  echo "  Database: $APP_DB_NAME"
  echo "  Username: $APP_DB_USER"
  echo "  Password: $APP_DB_PASS"
  echo ""
  echo "Admin access to postgres user via VM:"
  echo "  vagrant ssh"
  echo "  sudo su - postgres"
  echo ""
  echo "psql access to app database user via VM:"
  echo "  vagrant ssh"
  echo "  sudo su - postgres"
  echo "  PGUSER=$APP_DB_USER PGPASSWORD=$APP_DB_PASS psql -h localhost $APP_DB_NAME"
  echo ""
  echo "Env variable for application development:"
  echo "  DATABASE_URL=postgresql://$APP_DB_USER:$APP_DB_PASS@localhost:5433/$APP_DB_NAME"
  echo ""
  echo "Local command to access the database via psql:"
  echo "  PGUSER=$APP_DB_USER PGPASSWORD=$APP_DB_PASS psql -h localhost -p 5433 $APP_DB_NAME"
}

if pg_isready ;
  then
  echo "stoping server"
  pg_ctlcluster $PG_VERSION main stop
fi

pg_dropcluster $PG_VERSION main
pg_createcluster $PG_VERSION main --locale en_US.utf8
PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
PG_DIR="/var/lib/postgresql/$PG_VERSION/main"

# Edit postgresql.conf to change listen address to '*':
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"
sed -i "s/#max_locks_per_transaction = 64/max_locks_per_transaction = 640/" "$PG_CONF"
# Append to pg_hba.conf to add password auth:
echo "host    all             all             all                     md5" >> "$PG_HBA"
# Explicitly set default client_encoding
echo "client_encoding = utf8" >> "$PG_CONF"
pg_ctlcluster $PG_VERSION main start

cat << EOF | su - postgres -c psql
-- Create the database user:
CREATE USER $APP_DB_USER WITH PASSWORD '$APP_DB_PASS' SUPERUSER;
EOF

#-- Create the database:
cat << EOF | su - postgres -c psql
CREATE DATABASE "$APP_DB_NAME" WITH OWNER=$APP_DB_USER
                                  LC_COLLATE='en_US.UTF8'
                                  LC_CTYPE='en_US.UTF8'
                                  ENCODING='UTF8'
                                  TEMPLATE=template0;
EOF

cat << EOF | su - postgres -c psql
-- Create the database user:
CREATE USER vagrant SUPERUSER;
EOF

echo ""
print_db_usage
